package br.com.concretesolution.whatsapp.qa.fases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import br.com.concretesolution.qa.utils.SalvarEvidencia;
import br.com.concretesolution.qa.utils.Utils;

/**
 * @author Julio Cesar
 * Classe Resposanvel pelos obejtos da pagina resposavel New group
 *
 */

public class Newgroup {
	
	@FindBy(css="span.icon.icon-camera-light")
	private WebElement btnAddGroupIcon;
	
	@FindBy(xpath="/html/body/div/div/span[4]/div/ul/li[2]/a")
	private WebElement btnUpload;
	
	@FindBy(css="html.js.cssanimations.csstransitions.no-webp body div#app div.app-wrapper.app-wrapper-web.app-wrapper-main div.app.two div.drawer-manager span.pane.pane-one div span.flow-drawer-container div.drawer.drawer-new-group div.drawer-body.drawer-body-wide div.drawer-section div.text-input.active div.input-wrapper div.input-emoji div.input.input-text")
	private WebElement campoNomeGrupo;
	final WebDriver driver;

	public Newgroup(WebDriver driver) {
		this.driver = driver;
	}
	
	/**
	 * 
	 * @param nomeGrupo
	 * @param nomeImgemGrupo
	 */
	public void criarGrupo(String nomeGrupo,String nomeImgemGrupo){
		 btnAddGroupIcon.click();
		 if(Utils.isElementPresent( btnUpload, driver)){
			 
			 driver.switchTo().activeElement();
			 btnUpload.click();	 
			 Utils.indexarDocSemSikule(nomeImgemGrupo);
			 SalvarEvidencia.evidenciar(driver,"Evidencia upload doc");
		 }
	  
		 if(Utils.isElementPresent(campoNomeGrupo,driver)){
			 campoNomeGrupo.sendKeys(nomeGrupo); 
			 SalvarEvidencia.evidenciar(driver,"Grupo Criado");
		 }
		
	}
	
	

}
