package br.com.concretesolution.whatsapp.qa.fases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Settings {

	@FindBy(xpath="/html/body/div/div/div/div[1]/span[1]/div/span/div/div/div[2]/div[2]/div/div[2]/div/div/span")
	private WebElement btnChatWallpaper;
	
	final WebDriver  driver;
	public Settings(WebDriver driver) {
		this.driver = driver;
	}
	
	public Wallpaper clicaBtnChatWallpaper(){
		btnChatWallpaper.click();
		return PageFactory.initElements(driver,Wallpaper.class);
	}
	
}
