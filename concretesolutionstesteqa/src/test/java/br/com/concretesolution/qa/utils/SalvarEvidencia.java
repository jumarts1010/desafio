package br.com.concretesolution.qa.utils;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class SalvarEvidencia {
	private static File scrFile;
	private static Count n = new Count();
	private static String pathComplemento = null;
	private static String pathEvidencias = "C:\\Temp";
	private static DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
	private static Date date = new Date();
	private static String dataAtual = dateFormat.format(date).replace("/", "");
	private static String path;

	public static void evidenciar(WebDriver driver, String fase) {

		scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		if (pathComplemento == null || pathComplemento.equals("")) {
			path = pathEvidencias + "\\" + dataAtual + "\\";
		} else {
			path = pathEvidencias + "\\" + dataAtual + "\\" + pathComplemento
					+ "\\";
		}

		try {
			FileUtils.copyFile(scrFile, new File(path + fase + "\\" + fase
					+ "_" + n.getCount() + ".jpeg"));
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void evidenciarPDF(String fase) {

		if (pathComplemento == null || pathComplemento.equals("")) {
			path = pathEvidencias + "\\" + dataAtual + "\\";
		} else {
			path = pathEvidencias + "\\" + dataAtual + "\\" + pathComplemento
					+ "\\";
		}

		try {

			Robot robot = new Robot();
			robot.delay(5000);

			// Captura o screen shot da �rea definida pelo ret�ngulo
			BufferedImage bi = robot.createScreenCapture(new Rectangle(1600,
					750));
			ImageIO.write(bi, "jpg", new File(path + fase + "\\" + fase + "_"
					+ n.getCount() + ".jpeg"));

		} catch (AWTException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
