package br.com.concretesolution.qa.testes;


import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "classpath:cenariosPaper", tags = "@AlterarWallpaperWhatsapp", 
glue = "br.com.concretesolution.whatsapp.qa.steps.alterar.wallpaper", monochrome = true, dryRun = false)
public class AlterarWallpaperWhatsapp {
	

}
